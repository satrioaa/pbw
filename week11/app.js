list_angka = []
// fungsi untuk insert data 
// list_angka.push(1)
// list_angka.push(2)
// list_angka.push(3)
// list_angka.push(4)

function tambah(){
    var angka = document.getElementById("angka").value
    list_angka.push(angka)
    document.getElementById("list").innerText = list_angka
}

function jumlah(){
    var total = 0
    for (var i = 0; i < list_angka.length; i++){
        total += parseInt(list_angka[i])
    }
    document.getElementById("total").innerText = total
}

// fungsi looping untuk penjumlahan data
// jumlah = 0
// for (i = 0; i < list_angka.length; i++) {
//     jumlah = jumlah + list_angka[i] }
//i = 0   jumlah = 0 + 8 = 8
//i = 1   jumlah = 8 + 6 = 14
//i = 2   jumlah = 14 + 1 = 15
//i = 3   jumlah = 15 + 8 = 23
//i = 4   jumlah = 23 + 5 = 28
//i = 5   jumlah = 28 + 4 = 32
//i = 6   jumlah = 32 + 6 = 38
//i = 7   jumlah = 38 + 4 = 42
//i = 8   jumlah = 42 + 2 = 44
//i = 9   jumlah = 44 + 7 = 51

// fungsi untuk print data
//document.write(jumlah)
