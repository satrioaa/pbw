
<html lang="en">
    <head>
        <title>Formulir Dasar</title>
        <link rel="stylesheet" href="../assets/css/app.css">
    </head>
    <body>
        <h2>Formulir</h2>
        <form method="GET">
            <label>Nama Lengkap:</label>
            <input type="text" name="nama" placeholder="Masukkan Nama Lengkap" autocomplete="off" required size="50"> <br>
            <label>Jenis Kelamin: </label>
            <input type="radio" name="jk" value="1" checked> Laki-laki 
            <input type="radio" name="jk" value="2" > Perempuan
            <br>
            <label>Alamat: </label>
            <textarea name="alamat" cols="20" rows="5" placeholder="Masukkan Alamat anda"></textarea><br>
            <label>Hobi: </label>
            <input type="checkbox" name="hobi1" value="tidur"> Tidur
            <input type="checkbox" name="hobi2" value="turu"> Turu
            <input type="checkbox" name="hobi3" value="tilem"> Tilem
            <input type="checkbox" name="hobi4" value="bobo">Bobo
            <br>
            <label>Tanggal Lahir</label>
            <input type="date" name="tgl_lahir" min="2000-01-01" max="2003-12-31"> <br>
            <label>Warna</label>
            <input type="color" name="warna"> <br>
            <label>Pekerjaan</label>
            <select name="pekerjaan">
                <option disabled selected>-Pilih pekerjaan-</option>
                <option value="ngebot">ngebot</option>
                <option value="auto">auto</option>
                <option value="joki">joki</option>
            </select>
            <br>
            <label>Jurusan</label>
            <input type="text" list="jurusans" name="jurusan">
            <datalist id="jurusans">
                <option value="Teknik Informatika">Teknik Informatika </option>
                <option value="Sistem Informasi">Sistem Informasi </option>
                <option value="Teknik Kimia">Teknik Kimia </option>
                <option value="Matematika">Matematika</option>
                <option value="Fisika">Fisika </option>
                <option value="Teknik Elektro">Teknik Elektro </option>
            </datalist>
            <br>
            <label>Password</label>
            <input type="password" name="paswd">
            <input type="submit" value="Simpan">
            <input type="reset" value="Hapus isian">
        </form>
    </body>
    </html>